/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hcos.assignment1_tinocojaime;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jtinocotejeida
 */
public class OJTDAO {
    Connection conn = null;
    Statement stmt;
   
    /**
     * Default constructor will setup the connection and statement objects to be
     * used by this CamperDAO instance.
     *
     * @since 20180928
     * @author BJM
     */
    public OJTDAO() {
        try {
            
            String url = "jdbc:mysql://localhost:3306/ojt";
            conn = DriverManager.getConnection(url, "root", "");

            stmt = conn.createStatement();
        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }
    }

    /**
     * This method will select all campers, create camper objects for each, add
     * them to an ArrayList, then return the array list.
     *
     * @since 20180928
     * @author BJM
     */
    public ArrayList<OJTReflection> selectAll() {
        ArrayList<OJTReflection> studentsArray;
        try {
            //Next select all the rows and display them here...
            studentsArray = new ArrayList();

            ResultSet rs = stmt.executeQuery("select * from ojtreflection");
            while (rs.next()) {
                OJTReflection student = new OJTReflection();
                student.setStudentId(rs.getInt("studentId"));
                student.setStudentName(rs.getString(2));
                student.setReflectionText(rs.getString(3));
                studentsArray.add(student);
            }

            return studentsArray;
        } catch (SQLException ex) {
            Logger.getLogger(OJTDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * This method will return a particular camper.
     *
     * @since 20180928
     * @author BJM
     */
    public OJTReflection select(int studentId) {

        try {
            PreparedStatement theStatement = null;
            String sqlString = "select * from ojtreflection where studentId=?";
            theStatement = conn.prepareStatement(sqlString);
            theStatement.setInt(1, studentId);
            ResultSet rs = theStatement.executeQuery();            
            
            OJTReflection student = null;
            
            /*
            The camper is initially set to null.  Will loop through the result set
            to set the values of the new camper object.  If we don't find one then will
            be returning null.
            */

            while (rs.next()) {
                student = new OJTReflection();
                student.setStudentId(rs.getInt("studentId"));
                student.setStudentName(rs.getString(2));
                student.setReflectionText(rs.getString(3));
                
            }

            
            return student;
        } catch (SQLException ex) {
            Logger.getLogger(OJTDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * This method will delete the camper associated with the registration id.
     *
     * @param registrationId Identifies the camper to be deleted.
     * @since 20180928
     * @author BJM
     */
    public void delete(int studentId) {
        try {
            PreparedStatement deleteStatement = null;
            String sqlString = "delete from ojtreflection where studentId=?";
            deleteStatement = conn.prepareStatement(sqlString);
            deleteStatement.setInt(1, studentId);
            
            deleteStatement.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * This method will insert the input camper. It will notify the console if
     * an exception is encountered (IE camper already exists).
     *
     * @param camper The camper to insert
     * @since 20180928
     * @author BJM
     */
    public void insert(OJTReflection student) {
        try {

          //  String query = "INSERT INTO `Ojtreflection`(`studentId`, `studentName`, `reflection`) VALUES (";
         //   query += student.getStudentId()+ ",'" + student.getStudentName()+ "','" +  student.getReflectionText()+ "'";
         //   query += ");";
            
            PreparedStatement insertStatement = null;
            String sqlString = "INSERT INTO `ojtreflection`(`studentId`, `studentName`, `reflection`)"
                    + "VALUES (?,?,?)";
            insertStatement = conn.prepareStatement(sqlString);
            insertStatement.setInt(1, student.getStudentId());
            insertStatement.setString(2, student.getStudentName());
            insertStatement.setString(3, student.getReflectionText());
            

            
         //   System.out.println("About to query:" + query);
            insertStatement.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("Could not add camper");
        }

    }

    /**
     * Will update the database based on camper provided.
     *
     * @param camper Camper to update
     * @since 20180928
     * @author BJM
     */
    public void update(OJTReflection student) {
        try {

            String query = "UPDATE Ojtreflection SET "
                    + "studentName='"+ student.getStudentName()+"',reflection='"+student.getReflectionText()
                    +"' WHERE studentId="+student.getStudentId();
            System.out.println("About to query:" + query);
            stmt.executeUpdate(query);

        } catch (SQLException sqle) {
            System.out.println("Could not add Student");
        }
    }
}
