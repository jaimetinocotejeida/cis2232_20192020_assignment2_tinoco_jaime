/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hcos.assignment1_tinocojaime;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author jtinocotejeida
 */
public class MainClass {
    public static String MENU = "Menu Options:\nA) Add/Update Student\nS) Show Student\nX) Exit";
    public static final String PATH = "/cis2232/";
    public static String FILE_NAME = PATH + "reflection.json";

    /**
     * @param args the command line arguments
     */
  public static void main(String[] args) throws IOException {
        ArrayList<OJTReflection> theList = new ArrayList();
        OJTDAO ojtDAO = new OJTDAO();
        String option;
        do {
            System.out.println(MENU);
            option = Utility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    OJTReflection newStudent = new OJTReflection(true);
                    ojtDAO.insert(newStudent);
                    break;
                case "U":
                    //System.out.println("Picked A");
                    System.out.println("Enter Student id");
                    int studentIdForUpdate = Utility.getInput().nextInt();
                    Utility.getInput().nextLine(); //burn
                    OJTReflection student = ojtDAO.select(studentIdForUpdate);
                    if(student == null){
                        System.out.println("Student not found");
                    }else{
                        student.updateReflection();
                        ojtDAO.update(student);
                        System.out.println("student updated");
                    }
                    break;
                case "D":
                    //System.out.println("Picked A");
                    System.out.println("Enter student id");
                    int studentId = Utility.getInput().nextInt();
                    Utility.getInput().nextLine(); //burn
                    ojtDAO.delete(studentId);
                    break;
                case "S":
                    System.out.println("Here are the students");
                    theList = ojtDAO.selectAll();
                    for (OJTReflection eachStudent : theList) {
                        System.out.println(eachStudent);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }

        } while (!option.equalsIgnoreCase(
                "x"));
    }
    
   
    
    
}
